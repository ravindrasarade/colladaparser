#include <windows.h>
#include <stdio.h>
#include <vector>
#include <fstream>
#include <gl\glew.h> 
#include <gl/GL.h>
#include "vmath.h"

///////////////////////////
#include "ColladaReader.h"
#include "ModelDefinationManager.h"
#include "ForShader.h"
/////////////////////////

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace std;
using namespace vmath;

enum
{
  VDG_ATTRIBUTE_VERTEX = 0,
  VDG_ATTRIBUTE_COLOR,
  VDG_ATTRIBUTE_NORMAL,
  VDG_ATTRIBUTE_TEXTURE0,
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
ofstream  gpFile;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gShaderProgramObject;

GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;

GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

bool gbLight;

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
  // function declarations
  void initialize(void);
  void uninitialize(void);
  void display(void);

  // variable declaration
  WNDCLASSEX wndclass;
  HWND hwnd;
  MSG msg;
  TCHAR szClassName[] = TEXT("OpenGLPP");
  bool bDone = false;

  char logFilename[] = "output.txt";
  gpFile.open(logFilename, ios::out);
  if (gpFile.fail())
  {
    MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting "),
      TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
    exit(0);
  }
  else
  {
    gpFile << "***Log File Is Successfully Opened****" << endl;
  }

  // initializing members of struct WNDCLASSEX
  wndclass.cbSize = sizeof(WNDCLASSEX);
  wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  wndclass.cbClsExtra = 0;
  wndclass.cbWndExtra = 0;
  wndclass.hInstance = hInstance;
  wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndclass.lpfnWndProc = WndProc;
  wndclass.lpszClassName = szClassName;
  wndclass.lpszMenuName = NULL;

  // registering class
  RegisterClassEx(&wndclass);

  // create window
  hwnd = CreateWindow(szClassName,
    TEXT("OpenGL Programmable Pipeline Window"),
    WS_OVERLAPPEDWINDOW,
    100,
    100,
    WIN_WIDTH,
    WIN_HEIGHT,
    NULL,
    NULL,
    hInstance,
    NULL);

  ghwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  // initialize
  initialize();

  // message loop
  while (bDone == false)
  {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      if (msg.message == WM_QUIT)
        bDone = true;
      else
      {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    }
    else
    {
      // rendring function
      display();

      if (gbActiveWindow == true)
      {
        if (gbEscapeKeyIsPressed == true) //Continuation to glutLeaveMainLoop();
          bDone = true;
      }
    }
  }

  uninitialize();

  return((int)msg.wParam);
}

// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // function declarations
  void resize(int, int);
  void ToggleFullscreen(void);
  void uninitialize(void);

  // variable declarations
  static bool bIsAKeyPressed = false;
  static bool bIsLKeyPressed = false;

  // code
  switch (iMsg)
  {
  case WM_ACTIVATE:
    if (HIWORD(wParam) == 0)
      gbActiveWindow = true;
    else
      gbActiveWindow = false;
    break;
  case WM_ERASEBKGND:
    return(0);
  case WM_SIZE:
    resize(LOWORD(lParam), HIWORD(lParam));
    break;
  case WM_KEYDOWN:
    switch (wParam)
    {
    case VK_ESCAPE:
      if (gbEscapeKeyIsPressed == false)
        gbEscapeKeyIsPressed = true;
      break;
    case 0x46: // for 'f' or 'F'
      if (gbFullscreen == false)
      {
        ToggleFullscreen();
        gbFullscreen = true;
      }
      else
      {
        ToggleFullscreen();
        gbFullscreen = false;
      }
      break;
    case 0x4C: // for 'L' or 'l'
      if (bIsLKeyPressed == false)
      {
        gbLight = true;
        bIsLKeyPressed = true;
      }
      else
      {
        gbLight = false;
        bIsLKeyPressed = false;
      }
      break;
    default:
      break;
    }
    break;
  case WM_LBUTTONDOWN:
    break;
  case WM_CLOSE:
    uninitialize();
    break;
  case WM_DESTROY:
    PostQuitMessage(0);
    break;
  default:
    break;
  }
  return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
  // variable declarations
  MONITORINFO mi;

  // code
  if (gbFullscreen == false)
  {
    dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
    if (dwStyle & WS_OVERLAPPEDWINDOW)
    {
      mi = { sizeof(MONITORINFO) };
      if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
      {
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }
    ShowCursor(FALSE);
  }
  else
  {
    //code
    SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(ghwnd, &wpPrev);
    SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

    ShowCursor(TRUE);
  }
}

void initialize(void)
{
  // function declarations
  void uninitialize(void);
  void resize(int, int);

  // variable declarations
  PIXELFORMATDESCRIPTOR pfd;
  int iPixelFormatIndex;

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  // initialization of structure 'PIXELFORMATDESCRIPTOR'
  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
  pfd.iPixelType = PFD_TYPE_RGBA;
  pfd.cColorBits = 32;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.cDepthBits = 32;

  ghdc = GetDC(ghwnd);

  // choose a pixel format which best matches with that of 'pfd'
  iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
  if (iPixelFormatIndex == 0)
  {
    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;
  }

  // set the pixel format chosen above
  if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
  {
    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;
  }

  // create OpenGL rendering context
  ghrc = wglCreateContext(ghdc);
  if (ghrc == NULL)
  {
    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;
  }

  // make the rendering context created above as current n the current hdc
  if (wglMakeCurrent(ghdc, ghrc) == false)
  {
    wglDeleteContext(ghrc);
    ghrc = NULL;
    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;
  }

  GLenum glew_error = glewInit();
  if (glew_error != GLEW_OK)
  {
    wglDeleteContext(ghrc);
    ghrc = NULL;
    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;
  }

  /********************************************************************************************************/

  string xmlFile = "./Models/cube.dae"; //astroBoy_walk_Maya //model
  Model *model = new Model();
  //model->loadModel(xmlFile);
  gShaderProgramObject = ForShader::makeProgram(gpFile, "Shaders/VertexShader.vert", "Shaders/FragementShader.frag");
  
  /**************************************************************************************************/ 
  
    // getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
   // gNumVertices = getNumberOfSphereVertices();
  //gNumElements = getNumberOfSphereElements(); 

  glShadeModel(GL_SMOOTH);
  glClearDepth(1.0f); 
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glEnable(GL_CULL_FACE);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);  
  gPerspectiveProjectionMatrix = mat4::identity();
  gbLight = false;
  resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
  //code
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // start using OpenGL program object
  glUseProgram(gShaderProgramObject);

  if (gbLight == true)
  {
    
    glUniform1i(L_KeyPressed_uniform, 1);

    // setting light's properties
    glUniform3fv(La_uniform, 1, lightAmbient);
    glUniform3fv(Ld_uniform, 1, lightDiffuse);
    glUniform3fv(Ls_uniform, 1, lightSpecular);
    glUniform4fv(light_position_uniform, 1, lightPosition);

    // setting material's properties
    glUniform3fv(Ka_uniform, 1, material_ambient);
    glUniform3fv(Kd_uniform, 1, material_diffuse);
    glUniform3fv(Ks_uniform, 1, material_specular);
    glUniform1f(material_shininess_uniform, material_shininess);
  }
  else
  {
    // set 'u_lighting_enabled' uniform
    glUniform1i(L_KeyPressed_uniform, 0);
  }

  // OpenGL Drawing
  // set all matrices to identity
  mat4 modelMatrix = mat4::identity();
  mat4 viewMatrix = mat4::identity();

  // apply z axis translation to go deep into the screen by -2.0,
  // so that triangle with same fullscreen co-ordinates, but due to above translation will look small
  modelMatrix = translate(0.0f, 0.0f, -2.0f);

  glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
  glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
  glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

  // *** bind vao ***
  glBindVertexArray(gVao_sphere);

  // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
  glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

  // *** unbind vao ***
  glBindVertexArray(0);

  // stop using OpenGL program object
  glUseProgram(0);

  SwapBuffers(ghdc);
}

void resize(int width, int height)
{
  //code
  if (height == 0)
    height = 1;
  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
  //UNINITIALIZATION CODE
  if (gbFullscreen == true)
  {
    dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
    SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(ghwnd, &wpPrev);
    SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
    ShowCursor(TRUE);
  }
  // destroy vao
  if (gVao_sphere)
  {
    glDeleteVertexArrays(1, &gVao_sphere);
    gVao_sphere = 0;
  }
  // destroy position vbo
  if (gVbo_sphere_position)
  {
    glDeleteBuffers(1, &gVbo_sphere_position);
    gVbo_sphere_position = 0;
  }

  // destroy normal vbo
  if (gVbo_sphere_normal)
  {
    glDeleteBuffers(1, &gVbo_sphere_normal);
    gVbo_sphere_normal = 0;
  }

  // destroy element vbo
  if (gVbo_sphere_element)
  {
    glDeleteBuffers(1, &gVbo_sphere_element);
    gVbo_sphere_element = 0;
  }
  
  // delete shader program object
  glDeleteProgram(gShaderProgramObject);
  gShaderProgramObject = 0;

  //Deselect the rendering context
  wglMakeCurrent(NULL, NULL);

  //Delete the rendering context
  wglDeleteContext(ghrc);
  ghrc = NULL;

  //Delete the device context
  ReleaseDC(ghwnd, ghdc);
  ghdc = NULL;

  if (gpFile)
  {
    gpFile << "Log File Is Successfully Closed.\n";
    gpFile.close();
  }
}
