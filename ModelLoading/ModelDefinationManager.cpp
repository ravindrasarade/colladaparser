/****************************************************************************
10-Jul-2018    V1.0    RHS    $$1 Created
*****************************************************************************/

#include "ModelDefinationManager.h"


ModelDefinationManager::ModelDefinationManager()
{
  
}
/*****************************************************************************/

ModelDefinationManager::~ModelDefinationManager()
{
  
  deleteAllModelDefinitions();
}
/*****************************************************************************/

void ModelDefinationManager::addModelDefinition(Model *model)
{
  modelDefList.push_back(model);
}

/*****************************************************************************/
void ModelDefinationManager::deleteAllModelDefinitions()
{
  for (list <Model *>::iterator item = modelDefList.begin(); item != modelDefList.end(); ++item)
    delete (*item);
  modelDefList.clear();
}
/*****************************************************************************/


Model::Model()
{
  reader = new ColladaReader();
}


Model::~Model()
{
  deleteAllPartsInModel();
  delete reader;
}


/*****************************************************************************/
ModelPart* Model :: constructPart(string partID)
{
  ModelPart *mPart = new ModelPart[1];
  mPart->partID = partID;
  return mPart;
}

/*****************************************************************************/

void Model::addPartToModel(ModelPart *modelPart)
{
  modelPartList.push_back(modelPart);
}

/*****************************************************************************/
void Model::deleteAllPartsInModel()
{
  for (list <ModelPart *>::iterator item = modelPartList.begin(); item != modelPartList.end(); ++item)
    delete (*item);
  modelPartList.clear();
}


/*****************************************************************************/
/*Filled data in maps in reader class will be retrived here.
The maps will have key as position, normal, color etc.
Since map will store the data in sorted order.
Sequence of these geometry attributes has been stored attributeSequence.
And then this vector is looped and string stored will be used as a
search key in map.
This way same sequence will be maintained while storing the data in model.*/

bool Model :: setMeshContainer()
{
  map<string, GeometryData> geometryPartMap = reader->get_LibGeom_PartMap();
  vector<string> geometryPartIDSeqence = reader->get_LibGeom_PartIDSequence();
  int partcount = 0;
  for (vector<string> ::iterator geometryPartIDSeqItr = geometryPartIDSeqence.begin(); geometryPartIDSeqItr != geometryPartIDSeqence.end(); 
       geometryPartIDSeqItr++)
  {
    string geometryID = "";
    vector<ELEMENT> ElementContainer;
    vector<vector<GeometryEntity>> GeometryEntityContainer;
    
    ModelPart* part;

    map<string, GeometryData>::iterator geometryPartMapItr  = geometryPartMap.find(*geometryPartIDSeqItr);
    if (geometryPartMapItr != geometryPartMap.end())
    {
      geometryID = geometryPartMapItr->first;
      GeometryData geometry = geometryPartMapItr->second;

      part  = constructPart(geometryID);

      map<string, vector<int>> elementMap  = geometry.pElementData;
      map<string, vector<int>> elementInfo = geometry.pElementInfo;

      ///////////P Element Related information/////////////////////////////////////////    

      for (map<string, vector<int>> ::iterator itr = elementMap.begin(); itr != elementMap.end(); itr++)
      {
        string modelPartName = (*itr).first;
        vector<int> pElementRawData = (*itr).second;

        map<string, vector<int>> ::iterator itrInfo;
        for (itrInfo = elementInfo.begin(); itrInfo != elementInfo.end(); itrInfo++)
        {
          if ((*itrInfo).first == modelPartName)
          {
            break;
          }
          else if (itrInfo == elementInfo.end())
          {
            elementInfo.erase(itrInfo);
            return false;
          }
        }

        vector<int> infoVector = (*itrInfo).second;
        int nElementCnt = infoVector[0];
        int numVertices = infoVector[1];
        int numAttributes = infoVector[2];

        int numIndices = nElementCnt*numVertices * numAttributes;
        int nElementSize = numVertices * numAttributes;
        int index = 0;
        ElementContainer.resize(nElementCnt);

        for (int i = 1; i < nElementCnt + 1; i++)
        {

          vector<vector<int>> entityVect;
          entityVect.resize(numAttributes);
          Element element;
          element.numofAttributes = numAttributes;
          element.AttributeIDs.resize(numAttributes);

          /*we may have 2,3 or 4 entity attributes like Vertex,
          Normal, Texture Color etc.so setting them dynamically.
          element.AttributeIDs[0] will have VERTEX id vector
          element.AttributeIDs[1] will have NORMAL id vector
          element.AttributeIDs[2] will have TEXTURE id vector
          */

          for (int ii = 0; ii < numAttributes; ii++)
          {       
            entityVect[ii].resize(numVertices);
            element.AttributeIDs[ii].resize(numVertices);
          }
          int count = 0;
          for (int j = 0; j < nElementSize; j++)
          {
            if (numVertices == 3)
            {
              /*If there are 3 Verts, 3 Norms, 3 Text then elment size is 9, 
                entityVect[0] will have 3 verts at entityVect[0][0], entityVect[0][1], entityVect[0][2], similarly 
                entityVect[1] will have 3 norms at entityVect[1][0], entityVect[1][1], entityVect[1][2] and so on.
                so count is for second index of entityVect i.e. entityVect[][count].*/
              index = ((i - 1)*nElementSize + j) % numAttributes;
              if (j > 2 && index == 0)
                count++;
              entityVect[index][count] = pElementRawData[((i - 1)*nElementSize + j)];
            }
            else if (numVertices == 2)
            {
              index = ((i - 1)*nElementSize + j) % numAttributes;
              if (j == numAttributes)
                count++;
              entityVect[index][count] = pElementRawData[((i - 1)*nElementSize + j)];
            }
          }
          for (int ii = 0; ii < numAttributes; ii++)
          {
            element.AttributeIDs[ii] = entityVect[ii];
          }
          ElementContainer[i - 1] = element;
        }//end of for loop for elements
              
      }//map iterator loop
      /////////////////////End of P Elment Related Information////////////////
     

      //////////////////////Geometry Info////////////////////////
      map<string, vector<int>>    countStrideMap = geometry.countStrideMap;
      map<string, string>  geomAttributes = geometry.geometryAttributes; 
      vector<string> attributeSequence = geometry.attributeSequence;
      int NumOfGeomAttribs = geometry.numofGeomAttribs;
      GeometryEntityContainer.resize(NumOfGeomAttribs);

      if (countStrideMap.empty() || geomAttributes.empty())
      {
        return false;
      }

      int attributeNum = 0;
      for (int jj = 0; jj < attributeSequence.size(); jj++)
      {
        map<string, string> ::iterator itrGeomAttrb
          = geomAttributes.find(attributeSequence[jj]);

        map<string, vector<int>> ::iterator itrCount
          = countStrideMap.find(attributeSequence[jj]);

        vector<int> temp = (*itrCount).second;
        int attributeCount = temp[0];
        int attribStride = temp[1];

        if (attributeCount == 0)//sometimes there are no attributes provided but given only name 
          continue;

        if (itrGeomAttrb != geomAttributes.end() &&
          itrCount != countStrideMap.end())
        {
          string content = (*itrGeomAttrb).second;
          stringstream tokenizer;
          tokenizer.str(content);

          vector<double> tempVect;
          tempVect.resize(attributeCount*attribStride);
          for (int i = 0; i < (attributeCount*attribStride); i++)
          {
            tokenizer >> tempVect[i];
          }

          vector<GeometryEntity> geomEntites;
          geomEntites.resize(attributeCount);

          int entityID = 0;
          for (int i = 0; i < attributeCount; i++)
          {
            GeometryEntity newGeomEntity;
            newGeomEntity.entityCords = new double[attribStride];
            for (int j = 0; j < attribStride; j++)
            {
              newGeomEntity.entityCords[j] = tempVect[i*attribStride + j];
            }
            newGeomEntity.entityID = entityID;
            newGeomEntity.entityStride = attribStride;
            entityID++;
            geomEntites[i] = newGeomEntity;
          }
          GeometryEntityContainer[attributeNum] = geomEntites;
          attributeNum++;
        }
        else
        {
          return false;
        }
      }
      ////////////////////////////////////////////////////////////////////////
      part->GeometryEntityMap = GeometryEntityContainer;
      part->ElementMap = ElementContainer;
      addPartToModel(part);
      partcount++;
    }//end of if condition for geometryPartMap
  }//end of geometryPartIDSeqence for loop


  ////////////////Output////////////////////////////////////////////////////
  char elementOutPutFile[] = "./ElementOutput.txt";
  ofstream  elementFile;
  elementFile.open(elementOutPutFile, ios::out);
  
  char GeomOutPutFile[] = "./GeometryOutput.txt";
  ofstream  geomFile;
  geomFile.open(GeomOutPutFile, ios::out);
  
  
  list <ModelPart *> partList = getPartList();

  for (list <ModelPart *>::iterator itr = partList.begin(); itr != partList.end(); itr++)
  {
    elementFile << (*itr)->partID <<endl;
    geomFile << (*itr)->partID <<endl;

    vector<vector<GeometryEntity>> GeometryEntityContainer = (*itr)->GeometryEntityMap;
    int attributeCount = 0;
    for (vector<vector<GeometryEntity>>::iterator itrMain = GeometryEntityContainer.begin(); itrMain != GeometryEntityContainer.end(); itrMain++)
    {

      vector<GeometryEntity> GeometryEntityVect = (*itrMain);
     
      switch (attributeCount)
      {
     
      case 0:
        geomFile <<"vertices"<<endl;
        break;

      case 1:
        geomFile << "normals" << endl;
        break;

      case 2:
        geomFile << "textures" << endl;
         break;
      case 3:
        geomFile << "colours" << endl;
        break;

      default:
        geomFile << "Unknown" << endl;
        break;
      }
     
      for (vector<GeometryEntity>::iterator itrEnt = GeometryEntityVect.begin(); itrEnt != GeometryEntityVect.end(); itrEnt++)
      {
        GeometryEntity geomEntity = (*itrEnt);
        int      ntityID = geomEntity.entityID;
        int      ntityStride = geomEntity.entityStride;
        double  *ntityCords = geomEntity.entityCords;
        for (int jj = 0; jj < ntityStride; jj++)
        {
          geomFile << ntityCords[jj] << "    ";
        }
        geomFile << endl;
        delete ntityCords;
      }
      geomFile << "/*******************************************/" << endl;
      attributeCount++;
    }

    vector<ELEMENT> ElementContainer = (*itr)->ElementMap;
    for (vector<ELEMENT>::iterator itr = ElementContainer.begin(); itr != ElementContainer.end(); itr++)
    {
      int num = (*itr).numofAttributes;
      vector <vector<int>> attrIDs = (*itr).AttributeIDs;

      for (int ii = 0; ii < num; ii++)
      {
        for (vector<int>::iterator elemItr = attrIDs[ii].begin(); elemItr != attrIDs[ii].end(); elemItr++)
        {
          elementFile << (*elemItr) << "   ";
        }
      }
      elementFile << endl;
    }
  } 
  elementFile.close();
  geomFile.close();
  ///////////////////////////////////////////////////////////////////////////


  return true;
}

/*****************************************************************************/
void Model::loadModel(string xmlFile)
{
 // Model *model = NULL;
  reader->readColladaXMLFile(xmlFile);
  setMeshContainer();  
}

