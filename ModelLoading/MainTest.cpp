/****************************************************************************
30-Jun-2018    V1.0    RHS    $$1 Created
*****************************************************************************/

#include <iostream>
#include "ColladaReader.h"
#include "ModelDefinationManager.h"

using namespace std;

int main(int argc, char* args[])
{
  string xmlFile = "./SimpleLady.dae"; //astroBoy_walk_Maya //model
  Model *model = new Model();
  model->loadModel(xmlFile);
   return 0;
}