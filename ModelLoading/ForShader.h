#pragma once

#include <fstream>
#include <iostream>
#include <GL\glew.h>
using namespace std;

class ForShader
{
  public:

    ForShader();
    ~ForShader();

    static const GLchar* readShader(const char* filename);
    static GLuint makeProgram(ofstream &gFile, const char* vertex, const char* fragment);

};

