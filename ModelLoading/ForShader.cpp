#include "ForShader.h"


ForShader::ForShader()
{

}

ForShader::~ForShader()
{

}

const GLchar* ForShader::readShader(const char* filename)
{
    FILE* infile;
    fopen_s(&infile, filename, "rb");

    if (!infile) 
    {
      #ifdef _DEBUG
      std::cerr << "Unable to open file '" << filename << "'" << std::endl;
      #endif /* DEBUG */
      return NULL;
    }

    fseek(infile, 0, SEEK_END);
    int len = ftell(infile);
    fseek(infile, 0, SEEK_SET);

    GLchar* source = new GLchar[len + 1];
    fread(source, 1, len, infile);
    fclose(infile);
    source[len] = 0;
    return const_cast<const GLchar*>(source);
}

GLuint ForShader::makeProgram(ofstream &gFile, const char* vertex, const char* fragment)
{

  const GLubyte *glsl_version = glGetString(GL_SHADING_LANGUAGE_VERSION);
  gFile<< "GLSL version is:" << glsl_version <<endl;

  const char* vertexShaderCode   = readShader(vertex); 
  const char* fragmentShaderCode = readShader(fragment);

  //gFile<< vertexShaderCode << std::endl;
  //gFile<< fragmentShaderCode << std::endl;

  GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);

  GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
   
  glShaderSource(vertexShaderID, 1, &vertexShaderCode, 0);
  delete[] vertexShaderCode;

  glShaderSource(fragmentShaderID, 1, &fragmentShaderCode, 0);
  delete[] fragmentShaderCode;

  glCompileShader(vertexShaderID);

  GLint compiled;
  glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &compiled); 
  if (compiled == GL_FALSE) 
  {
    GLsizei len;
    glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &len); 

    GLchar* log = new GLchar[len + 1]; 
    glGetShaderInfoLog(vertexShaderID, len, &len, log); 
    gFile<< "Vertex Shader compilation failed: " <<log <<endl;
    delete[] log;
  }
  else 
  { 
    gFile<< "Vertex Shader compiled " <<endl;
  }
  ///////////////////////////////////////////////

  glCompileShader(fragmentShaderID);

  glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &compiled); 
  if (compiled == GL_FALSE)
  {
     GLsizei len;
     glGetShaderiv(fragmentShaderID, GL_INFO_LOG_LENGTH, &len);
    
     GLchar* log = new GLchar[len + 1]; 
     glGetShaderInfoLog(fragmentShaderID, len, &len, log);
     gFile<< "Fragment Shader compilation failed: " << log <<endl;
     delete[] log;
   }
   else
   { 
     gFile<< "Fragment Shader compiled " <<endl;
   }
   
   GLuint programID = glCreateProgram();
   glAttachShader(programID, vertexShaderID);
   glAttachShader(programID, fragmentShaderID);
   glLinkProgram(programID);

   ///////////////////////////////////////////////
    GLint linked;
    glGetProgramiv(programID, GL_LINK_STATUS, &linked);
    if (linked == GL_FALSE)
    {
      GLsizei len;
      glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &len);

      GLchar* log = new GLchar[len + 1];
      glGetProgramInfoLog(programID, len, &len, log);
      gFile<< "glLinkProgram(programID) failed: " << log << std::endl;
      delete[] log;
    }
    ///////////////////////////////////////////////

    //glDetachShader(programID, vertexShaderID); 
    glDeleteShader(vertexShaderID);
    //glDetachShader(programID, fragmentShaderID);
    glDeleteShader(fragmentShaderID);


    return programID;
}