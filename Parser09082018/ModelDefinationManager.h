#pragma once
/****************************************************************************
10-Jul-2018    V1.0    RHS    $$1 Created
*****************************************************************************/

#include <vector>
#include <list>
#include <sstream>
#include <algorithm> 
#include <map>
#include <utility>
#include <fstream>
#include "ColladaReader.h"


using namespace std;

struct GeometryEntity
{
  int      entityID;
  int      entityStride;
  double  *entityCords;
};

struct ELEMENT
{
  int numofAttributes;
  vector <vector<int>> AttributeIDs;
};

struct ModelPart
{
  string partID;
  vector<vector<GeometryEntity>> GeometryEntityMap;
  vector<ELEMENT> ElementMap;
};


typedef  ELEMENT Element;
typedef  int STATUS;

class Model
{

private:
   ColladaReader *reader;
   list <ModelPart *> modelPartList;
 

public:
  Model();
  ~Model();
  
  void loadModel(string xmlFile);

  ModelPart* constructPart(string partID);
  void addPartToModel(ModelPart *modelPart);
  list <ModelPart *> getPartList() {return modelPartList; }
  void deleteAllPartsInModel();
  
 /* map < string, vector<vector<GeometryEntity>>> getGeometryEntityContainer()
  {
    return GeometryEntityMap;
  }  
  
  map < string, vector<ELEMENT>> getElementContainer()
  {
    return ElementMap;
  }*/

  bool setMeshContainer();

};


class ModelDefinationManager
{

private:
  list <Model *> modelDefList;

public:
  ModelDefinationManager();
  virtual ~ModelDefinationManager();
  void addModelDefinition(Model *model);
  void deleteAllModelDefinitions();
  //Model getDefinitions();

};


/*Note:
Depend upon number of attributes such as Vertex, Normal ,
Texture, Color for each model, GeometryEntity will be filled
in a vector.Such list of vectors will be kept in GeometryEntityContainer
So first vector will be Vertex and then Normal and so on.
In some models we may have more than one geometry so this has been
stored against the geometry ID given in collada file.*/